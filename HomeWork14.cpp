﻿
#include <iostream>
#include <string>


int main()
{
    std::string str{"Skillbox!"};

    std::cout << "String: " << str << std::endl;
    int length = str.length();
    std::cout << "String length: " << length << std::endl;
    std::cout << "First symbol: " << str[0] << std::endl;
    std::cout << "Last symbol: " << str[length - 1] << std::endl;

    return 0;
}
